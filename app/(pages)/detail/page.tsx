export default function Detail() {
  return (
    <main>
      <h1>我是详情页面</h1>
      <div class="space-x-2 border border-red">
        <div class="inline-block">01</div>
        <div class="inline-block">02</div>
        <div class="inline-block">03</div>
      </div>
      <p class="text-sm">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde officia.
      </p>
      <p class="text-base">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde officia.
      </p>
      <p class="text-md">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde officia.
      </p>
      <p class="text-[16px]">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde officia.
      </p>
      <p class="text-lg">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde officia.
      </p>
      <p class="text-xl">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde officia.
      </p>
    </main>
  );
}
